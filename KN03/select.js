db.Hero.find(
    {
        Birthday: {
            $gt: ISODate("2007-01-01"),
            $lt: ISODate("2030-12-31")
        }
    }
);

db.Map.find(
    {
        $or: [
            {"Name": "Busan"},
            {"Location": "London, United Kingdom"}
        ]
    }
);

db.Hero.find(
    {
        $and: [
            {"Hero_is_good_on_Map": {
                $all: [ObjectId("666166f6c2b17d0900db1076")]
            }},
            {"Weapon": "Particle Cannon"}
        ]
    }
);

db.Hero.find(
    {
        $and: [
            {"Hero_is_good_on_Map": {
                $all: [ObjectId("666166f6c2b17d0900db1076")]
            }},
            {"Weapon": "Particle Cannon"}
        ]
    },
    {
        '_id': false
    }
    
);

db.Hero.find(
    {
        "Name": {
            $regex : /(?:[^aA]*[aA][^aA]*){2}[^aA]*/
        }
    },
    {
        '_id': false
    }
    
);

