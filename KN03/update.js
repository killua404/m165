db.Hero.updateOne(
    
    { _id: ObjectId("666166f7c2b17d0900db107a") }, 
    { $set: { "Category": new ObjectId("666166f6c2b17d0900db1077") } }

);

db.Map_Type.updateMany(
    { $or: [{ "Name": "Control" }, { "Name": "Flashpoint" }] },
    { $set: { "isAsynchron": true } }
);

db.Hero_Category.replaceOne(
    { "Name": "DPS" },
    { "Name": "Damage", "Passive": "None" }
);
