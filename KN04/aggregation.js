db.Hero.aggregate(
         [
            {
                $match: {
                    "Hero_is_good_on_Map": ObjectId("666ab2e0c33ffa9866dee4b3")
                }
                
            },
            { $match: {"Weapon": "Particle Cannon"}}
        ]
);

db.Map_Type.aggregate(
    [
        {
            $match: {
                "isAsynchron": false
            }
        },
        {
            $project: {"Name":1}
        },
        {
            $sort: {"Name":1}
        }

    ]
);

db.Map_Type.aggregate(
    [
        {$count : "Total Types:"}
    ]
);

// B)

db.Hero.aggregate(
    [
        {
            $project: {
              numAbilities: { $size: "$Abilities" }
            }
        },
        {
            $group: {
                _id: null,
                "avgAbilities": {$avg: "$numAbilities"},
            }
        }
    ]
);

db.Map.aggregate(
    [
        {
            $lookup: {
                from: "Map_Type",
                localField: "Type",
                foreignField: "_id",
                as: "Map_Type"
            }
        }
    ]
);

db.Map.aggregate(
    [
        {
            $lookup: {
                from: "Map_Type",
                localField: "Type",
                foreignField: "_id",
                as: "Map_Type"
            }
        }, 
        {
            $match : {
                "Map_Type.isAsynchron" : true


            }
        }
        

    ]
);

// C)

db.Hero.aggregate(
    [
        {$unwind: "$Abilities"},
        {$project: {
            _id : false,
            Name : "$Abilities.Name",
            Description : "$Abilities.Description",
            isPassive : "$Abilities.isPassive",
            isUltimate : "$Abilities.isUltimate"
            }
        }
    ]
);

db.Hero.aggregate(
    [
        {$unwind: "$Abilities"},
        {$match : {
            "Abilities.isUltimate" : true
        }},
        {$project: {
            _id : false,
            Name : "$Abilities.Name",
            Description : "$Abilities.Description",
            isPassive : "$Abilities.isPassive",
            isUltimate : "$Abilities.isUltimate"
            }
        }
    ]
);