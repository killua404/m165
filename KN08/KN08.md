# KN08

## A)

[script](./script.txt)

## B)

### All

```

MATCH (n)
RETURN n;

```

OPTIONAL MATCH erlaubt Muster, die unter Umständen nicht komplett sind.

```

OPTIONAL MATCH (n)-[r]->(m)

```

Matcht Knoten mit Verbindung, es kann aber z.B. sein, dass r nicht existiert.

### 1 Alle Helden und deren Abilities

Alle Helden und deren Abilities finden, inklusiv der Properties isUltimate und isPassive.

```

MATCH (h:Hero)-[:has]->(a:Ability)
RETURN h.Name AS Hero, a.Name AS Ability, a.isPassive AS Passive, a.isUltimate AS Ultimate;


```

### 2 Heldenkategorie und Liste der Helden


```

MATCH (c:Hero_Category)<-[:isPartOf]-(h:Hero)
RETURN c.Name AS Category, collect(h.Name) AS Heroes;

```

### 3 Alle Helden gut auf Map

Alle Helden die auf King's Row gut sind und eine Winrate grösser als 55 haben.

```

MATCH (h:Hero)-[r:is_good_on]->(m:Map {Name: "King's Row"})
WHERE r.Winrate > 55
RETURN h.Name AS Hero, r.Winrate AS Winrate, m.Name AS Map;

```

### 4 Map Asynchron und in South Korea oder in Veracruz, Mexico

```

MATCH (m:Map)-[:isPartOf]->(mt:Map_Type)
WHERE mt.isAsynchron = true AND (m.Location = "South Korea" OR m.Location = "Veracruz, Mexico")
RETURN mt.Name AS MapType, collect(m.Name) AS Maps;

```

## c)

Vorher:

![alt text](image.png)

```

MATCH (h:Hero {Name: "Hanzo"})
DELETE h;

```

Nachher 1:

![alt text](image-1.png)


```

MATCH (h:Hero {Name: "Hanzo"})
DETACH DELETE h;

```

Nachher 2:

![alt text](image-2.png)

```

MATCH (h:Hero {Name: "Hanzo"})
RETURN h;


```

Existiert nicht mehr.

![alt text](image-3.png)

## D)

### 1 Winrate aktualisieren

Die WInrate von Hanzo wird auf der Map Kingsrow von 53 auf 58 aktualisiert.

```

MATCH (h:Hero {Name: "Hanzo"})-[r:is_good_on]->(m:Map {Name: "King's Row"})
SET r.Winrate = 58
RETURN h.Name, m.Name, r.Winrate;

```

### 2 Ändern der Beschreibung einer Ability

Ändert die Beschribung der Ability Graviton Surge

```

MATCH (a:Ability {Name: "Graviton Surge"})
SET a.Description = "Big Gravity Hole"
RETURN a.Name, a.Description;

```

### 3 Ändern der Kanten

Ändert die Kante von Hanzo so, dass er neu in der Support Kategorie ist.

```

MATCH (h:Hero {Name: "Hanzo"})-[r:isPartOf]->(c:Hero_Category {Name: "DPS"})
MATCH (Category:Hero_Category {Name: "Support"})
DELETE r
CREATE (h)-[:isPartOf]->(Category)
RETURN h.Name, Category.Name;

```

## E)

### Merge

Merge ist eine kombination aus Match und Create. In diesem Beispiel wird sichergestellt, dass eine Verbindung von Zarya zu Dorado besteht.
Falls die Verbindung schon existiert wird die Winrate um 1 erhöht, wenn sie neu erstellt werden musste, ist der default Wert 50.

```
MATCH (h:Hero {Name: "Zarya"}), (m:Map {Name: "Dorado"})
MERGE (h)-[r:is_good_on]->(m)
ON CREATE SET r.Winrate = 50
ON MATCH SET r.Winrate = r.Winrate + 1
RETURN h.Name, m.Name, r.Winrate;
```

### Foreach

Mit foreach kann man über eine Liste iterieren.
In diesem Beispiel wird jedem Helden eine eigene Basic Ability vergeben, falls diese nicht schon existiert.

```

MATCH (h:Hero)
WITH h, collect(h) AS heroes
FOREACH (hero IN heroes | 
    MERGE (hero)-[:has]->(basic:Ability {Name: "Basic Attack", Description: "A basic attack ability.", isPassive: false, isUltimate: false})
)
RETURN heroes;

```